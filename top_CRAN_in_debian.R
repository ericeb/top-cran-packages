
library(cranlogs)
library(R.utils)


# get Debian unstable R packages as vector
get_debian_cran_packages <- function(deburl = "https://packages.debian.org/unstable/allpackages?format=txt.gz") {
  download.file(url = deburl, destfile = "unstable.txt.gz")
  unstable_txt <- gunzip("unstable.txt.gz")
  unstable <- readLines(unstable_txt)
  unstable <- strsplit(unstable, split = " ")
  unstable <- unstable[-c(1:6)]
  packages <- unlist(lapply(unstable, `[[`, 1))
  return(packages[grep(packages, pattern = "r-cran")])
}

top_cran <- function(count, period="last-month") {
  if (count > 100) count <- 100
  month <- cranlogs::cran_top_downloads(when = period, count = count)$package
  return(paste0(rep("r-cran-", count), tolower(month)))
}

missing_in_debian <- function(count, period, debian = NULL, debianURL = NULL) {
 if (is.null(debian)) {
   debian <- get_debian_cran_packages(debianURL)
   }
 cran <- top_cran(count, period)
 return(cran[!(cran %in% debian)])
}


unstable <- get_debian_cran_packages()

missing_in_debian(100, "last-day", unstable)
missing_in_debian(100, "last-week", unstable)
missing_in_debian(100, "last-month", unstable)
